# -*- coding: utf-8 -*-
"""
Created on Sat Mar 10 11:34:37 2018

@author: Darío Pascual

https://www.openml.org/d/1492
"""

import pandas as pd

import sys
sys.path.append("functions")
import train_test as tt
import aux_functions as af
import create_models as cm

plants = pd.read_csv("../data/plants_shape/one-hundred-plants-shape.csv")

#grouped_clases = tt.group_by_class(plants, -1)

test_train = tt.create_train_test(plants, 0.2, -1)

test = test_train["test"]
train = test_train["train"]

y_test = test.iloc[:,-1].values
y_train = train.iloc[:,-1].values

X_test = test.iloc[:,0:test.shape[1]-1]
X_train = train.iloc[:,0:train.shape[1]-1]

results = cm.create_models(X_train, y_train, X_test, y_test)

af.load_new_results('../results.xml', 'results', 'plants_shape', results)
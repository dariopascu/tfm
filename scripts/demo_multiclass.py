# -*- coding: utf-8 -*-
"""
Created on Wed Jan 31 19:58:44 2018

@author: dario
http://scikit-learn.org/stable/modules/multiclass.html
"""
import pandas as pd
import sklearn.multiclass as skm
from sklearn.svm import LinearSVC
from sklearn import tree

letters = pd.read_csv("../data/letter/letter-recognition.data")

letters.head()

target = letters.loc[:,"lettr"]
y = target.values

data = letters.iloc[:,1:letters.shape[1]-2]
X = data.values

def compare_results(y, result):
    return((sum(y==result)/len(y))*100)
    
"""
One-vs-All.
"""
result_OVA = skm.OneVsRestClassifier(LinearSVC(random_state=0)).fit(X, y).predict(X)    
compare_results(y,result_OVA)

"""
One-vs-One
"""
result_OVO = skm.OneVsOneClassifier(LinearSVC(random_state=0)).fit(X, y).predict(X)
compare_results(y,result_OVO)
    
"""
C4.5
"""
clf = tree.DecisionTreeClassifier()
result_Tree = clf.fit(X,y).predict(X)
compare_results(y,result_Tree)
# -*- coding: utf-8 -*-
"""
Created on Tue Feb 20 20:30:03 2018

@author: Darío Pascual
"""

import random
import pandas as pd

random.seed(1)

"""
"""
def group_by_class(data,class_position):
    
    grouped_clases = {el:list() for el in set(data.iloc[:,class_position])}
    
    for i in range(0,len(data)):
        class_value = data.iloc[i,class_position]
        grouped_clases[class_value].append(i)
    return(grouped_clases)
        

def create_train_test(data, test_percent, class_position):
    
    test = pd.DataFrame(columns=list(data))
    train = pd.DataFrame(columns=list(data))
    
    grouped_clases = group_by_class(data, class_position)
 
    for i in grouped_clases.values():
        num_test_elements = round(len(i)*test_percent)
        
        random_positions = random.sample(i, len(i))
        test_possitions = random_positions[0:num_test_elements]
        train_possitions = random_positions[num_test_elements:]
        
        for tst in data.iloc[test_possitions,:].values:
            test.loc[-1] = tst.tolist()
            test.index = test.index + 1
            
        for trn in data.iloc[train_possitions,:].values:
            train.loc[-1] = trn.tolist()
            train.index = train.index + 1
        
    return{'train':train,'test':test}
    
def compare_results(y, result):
    return((sum(y==result)/len(y))*100)
# -*- coding: utf-8 -*-
"""
Created on Sat Mar 10 10:28:14 2018

@author: Darío Pascual
"""

import xml.etree.cElementTree as ET
import os

def create_xml(file_path):
    root = ET.Element('results')
    tree = ET.ElementTree(root)
    tree.write(file_path)

def append(file_path, root_node, dataset_name, results):
    tree = ET.parse(file_path)
    root = tree.getroot()
    
    doc = ET.SubElement(root, dataset_name)
    method = 1
    for i in results.items():
        ET.SubElement(doc, 'method_' + str(method), name= str(i[0])+'_Acc').text = str(i[1])
        method = method + 1
    
    writeEl = ET.ElementTree(root)
    writeEl.write(file_path)
"""
load_new_results('../results.xml', 'results', 'plants', res) 
"""   
def load_new_results(file_path, root_node, dataset_name, results):
    if not os.path.isfile(file_path):
        create_xml(file_path)

    append(file_path, root_node, dataset_name, results)
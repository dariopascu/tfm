# -*- coding: utf-8 -*-
"""
Created on Sat Mar 10 11:40:39 2018

@author: Darío Pascual
"""

import sklearn.multiclass as skm
from sklearn import tree
import train_test as tt

def create_models(X_train, y_train, X_test, y_test):
    results = {}

    """
    One-vs-All.
    """
    result_OVA = skm.OneVsRestClassifier(tree.DecisionTreeClassifier()).fit(X_train, y_train).predict(X_test)
    OVA_Acc = tt.compare_results(y_test,result_OVA)
    print(OVA_Acc)
    results.update({'OVA':OVA_Acc})
    
    
    """
    One-vs-One
    """
    result_OVO = skm.OneVsOneClassifier(tree.DecisionTreeClassifier()).fit(X_train, y_train).predict(X_test)
    OVO_Acc = tt.compare_results(y_test,result_OVO)
    print(OVO_Acc)
    results.update({'OVO':OVO_Acc})
    
    """
    TreeClassifier
    """
    clf = tree.DecisionTreeClassifier()
    result_Tree = clf.fit(X_train, y_train).predict(X_test)
    Tree_Acc = tt.compare_results(y_test,result_Tree)
    print(Tree_Acc)
    results.update({'Tree':Tree_Acc})
    
    return results
# -*- coding: utf-8 -*-
"""
Created on Mon Feb 19 11:49:11 2018

@author: Darío Pascual
"""

"""
Functions
"""

import pandas as pd
import sklearn.multiclass as skm
from sklearn.svm import LinearSVC
from sklearn import tree

def unpickle(file):
    import pickle
    with open(file, 'rb') as fo:
        dict = pickle.load(fo, encoding='bytes')
    return dict

def compare_results(y, result):
    return((sum(y==result)/len(y))*100)

"""
Load data
"""
meta = unpickle('data/cifar-100/meta')
train = unpickle('data/cifar-100/train')
test = unpickle('data/cifar-100/test')

train_t = pd.Series(str(x) for x in train[b'fine_labels'])
train_y = train_t.values

test_t = pd.Series(str(x) for x in test[b'fine_labels'])
test_y = test_t.values


train_X = train[b'data']
test_X = test[b'data']

"""
One-vs-All.
"""
result_OVA = skm.OneVsRestClassifier(LinearSVC(random_state=0)).fit(train_X, train_y).predict(test_X)    
res = compare_results(test_y,result_OVA)

with open('data/results.txt', 'a') as f:
    f.write("Resultados OVA: " + str(res) + '\n')

"""
One-vs-One
"""
result_OVO = skm.OneVsOneClassifier(LinearSVC(random_state=0)).fit(train_X, train_y).predict(test_X)
res = compare_results(test_y,result_OVO)

with open('data/results.txt', 'a') as f:
    f.write("Resultados OVO: " + str(res) + '\n')
    
"""
C4.5
"""
clf = tree.DecisionTreeClassifier()
result_Tree = clf.fit(train_X,train_y).predict(test_X)
res = compare_results(test_y,result_Tree)

with open('data/results.txt', 'a') as f:
    f.write("Resultados trees: " + str(res) + '\n')